<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Tag extends Model {

        protected $fillable = ['title'];

        /**
         * The articles that belong to the Tag.
         */
        public function articles() {
            return $this->belongsToMany('App\Article');
        }

    }
    