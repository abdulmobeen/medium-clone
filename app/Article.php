<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Article extends Model {

        protected $fillable = ['title', 'description', 'image_url'];
    
        /**
         * The tags that belong to the Article.
         */
        public function tags() {
            return $this->belongsToMany('App\Tag');
        }

    }
    