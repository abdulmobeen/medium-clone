<?php

    namespace App\Http\Controllers;

    use App\Article;
    use Illuminate\Http\Request;

    class ArticleController extends Controller {

        /**
         * Enforce restrictions
         */
        public function __construct() {
            // Admin can only creat the articles
            $this->middleware('auth', ['except' => ['index', 'show']]);
        }

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index() {
            $articles = Article::with('tags')->paginate(5);
            return view('article.index')->with('articles', $articles);
        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id) {
            $data = Article::with('tags')->where('id', $id)->get()->first();

            return view('article.show')->with('article', $data);
        }

    }
    