<!DOCTYPE html>
<html lang="en">

    <head>
        @include('layouts.partials.header')
    </head>

    <body>

        <!-- Navigation -->
        @include('layouts.partials.nav')

        <!-- Page Content -->
        <div class="container">

            <!-- /.content -->
            @yield('content')

        </div>
        <!-- /.container -->

        <!-- Footer -->
        @include('layouts.partials.footer')



        <!-- Bootstrap core JavaScript -->
        @include('layouts.partials.footer-scripts')

    </body>

</html>
