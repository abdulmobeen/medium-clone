@extends('layouts.mainlayout')

@section('content')

      <div class="row">

    <!-- Blog Entries Column -->
    <div class="col-md-8">

        <h1 class="my-4">Lattest Dummy Articles
            <small>Secondary Text</small>
        </h1>

        @foreach ($articles as $article)
        <!-- Blog Post -->
        <div class="card mb-4">
            <img class="card-img-top" src="{{$article->image_url}}" alt="Card image cap">
            <div class="card-body">
                <h2 class="card-title">{{$article->title}}</h2>
                <p class="card-text">{{str_limit($article->description,150)}}</p>
                <a href="{{ route('articles.show', $article->id) }}" class="btn btn-primary" >Read More &rarr;</a>
            </div>
            <div class="card-footer ">

                @if($article->tags->count() > 0)

                @foreach ($article->tags as $tag)
                <a href="{{ route('tags.show', $tag->id) }}"class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">{{$tag->name}}</a>
                @endforeach
                @endif
            </div>
        </div>
        @endforeach


        <ul class="pagination justify-content-center mb-4">
            {{$articles->links('vendor.pagination.bootstrap-4')}}
        </ul>

    </div>

    <!-- Sidebar Widgets Column -->
    <div class="col-md-4">

        <!-- Search Widget -->
        <div class="card my-4">
            <h5 class="card-header">Search</h5>
            <div class="card-body">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>

        <!-- Categories Widget -->
        <div class="card my-4">
            <h5 class="card-header">Categories</h5>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="list-unstyled mb-0">
                            <li>
                                <a href="#">Web Design</a>
                            </li>
                            <li>
                                <a href="#">HTML</a>
                            </li>
                            <li>
                                <a href="#">Freebies</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul class="list-unstyled mb-0">
                            <li>
                                <a href="#">JavaScript</a>
                            </li>
                            <li>
                                <a href="#">CSS</a>
                            </li>
                            <li>
                                <a href="#">Tutorials</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Side Widget -->
        <div class="card my-4">
            <h5 class="card-header">Side Widget</h5>
            <div class="card-body">
                You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!
            </div>
        </div>

    </div>


    @endsection






    <b></b>