<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;

    class DatabaseSeeder extends Seeder {

        protected $toTruncate = ['users','articles','tags','article_tag'];

        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run() {


            Model::unguard();

            foreach ($this->toTruncate as $table) {
                DB::table($table)->truncate();
            }


            $this->call(UsersTableSeeder::class);

            factory(App\Tag::class, 20)->create();

            
            factory(App\Article::class, 20)->create()->each(function($article) {

                $attach_tags = random_int(0, 1);

                $all_tags = range(1, 20);


                shuffle($all_tags);

                if ($attach_tags) {

                    $selected_tags = array_slice($all_tags, 0, 3);
                    $article->tags()->attach($selected_tags);
                }
            });

            Model::reguard();
        }

    }
    