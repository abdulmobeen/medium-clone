<?php
use Carbon\Carbon;
    /*
      |--------------------------------------------------------------------------
      | Model Factories
      |--------------------------------------------------------------------------
      |
      | Here you may define all of your model factories. Model factories give
      | you a convenient way to create models for testing and seeding your
      | database. Just tell the factory how a default model should look.
      |
     */

    /** @var \Illuminate\Database\Eloquent\Factory $factory */
    $factory->define(App\User::class, function (Faker\Generator $faker) {
        static $password;

        return [
            'name' => $faker->name,
            'email' => $faker->unique()->safeEmail,
            'password' => $password ?: $password = bcrypt('secret'),
            'remember_token' => str_random(10),
        ];
    });


    // Tag Model 
    $factory->define(App\Tag::class, function (Faker\Generator $faker) {
        return [
            'name' => $faker->name,
        ];
    });


    // Tag Article 
    $factory->define(App\Article::class, function (Faker\Generator $faker) {
//        $filepath = storage_path('images');
//
//        if (!File::exists($filepath)) {
//            File::makeDirectory($filepath);
//        }

        return [
            'title' => $faker->word(),
//            'description' => $faker->paragraph(),
            'description' => $faker->text(600),
//            'image_url' => $faker->image(750, 300),
            'image_url' => $faker->imageUrl(640, 300),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ];
    });
    